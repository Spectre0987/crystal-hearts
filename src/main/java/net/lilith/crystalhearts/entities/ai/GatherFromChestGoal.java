package net.lilith.crystalhearts.entities.ai;

import net.lilith.crystalhearts.entities.GolemEntity;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.ai.goal.Goal;

import java.util.EnumSet;
import java.util.function.Function;

public class GatherFromChestGoal extends Goal {

    private GolemEntity golem;
    private Function<GolemEntity, BlockPos> gatherPos;

    public GatherFromChestGoal(GolemEntity entity, Function<GolemEntity, BlockPos> gatherPos){
        this.golem = entity;
        this.gatherPos = gatherPos;
        this.setFlags(EnumSet.of(Flag.LOOK, Flag.MOVE));
    }

    @Override
    public boolean canUse() {
        //return this.;
        return false;
    }
}
