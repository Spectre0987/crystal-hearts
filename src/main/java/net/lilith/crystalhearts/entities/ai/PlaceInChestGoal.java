package net.lilith.crystalhearts.entities.ai;

import net.lilith.crystalhearts.entities.GolemEntity;
import net.lilith.crystalhearts.misc.ObjectHolder;
import net.lilith.crystalhearts.network.Network;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.items.CapabilityItemHandler;

import java.util.EnumSet;

public class PlaceInChestGoal extends Goal {

    private GolemEntity golem;

    public PlaceInChestGoal(GolemEntity entity){
        this.golem = entity;
        this.setFlags(EnumSet.of(Flag.LOOK, Flag.MOVE));
    }

    @Override
    public boolean canUse() {
        //If has chest
        if(this.golem.getChestPos() != null && !this.golem.getChestPos().equals(BlockPos.ZERO)){
            //if has inv contents
            for(int slot = 0; slot < this.golem.getInventory().getSlots(); ++slot){
                if(!this.golem.getInventory().getStackInSlot(slot).isEmpty())
                    return true;
            }
        }

        return false;
    }

    @Override
    public void tick() {
        super.tick();

        BlockPos chest = this.golem.getChestPos();
        //Set navigation
        if(this.golem.getNavigation().isDone() || this.golem.getNavigation().isStuck()){
            this.golem.getNavigation().moveTo(chest.getX() + 0.5, chest.getY(), chest.getZ() + 0.5, 1.0F);
        }

        //Empty inv
        ObjectHolder<Boolean> changedInv = new ObjectHolder<>(false);
        if(this.golem.getOnPos().distSqr(chest) < 1.5*1.5){
            BlockEntity tile = this.golem.getLevel().getBlockEntity(chest);
            if(tile != null){
                tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(cap -> {
                    for(int slot = 0; slot < this.golem.getInventory().getSlots(); ++slot){
                        for(int cSlot = 0; cSlot < cap.getSlots(); ++cSlot){
                            ItemStack old = golem.getInventory().getStackInSlot(slot).copy();

                            ItemStack remainder = cap.insertItem(cSlot, golem.getInventory().getStackInSlot(slot), false);
                            golem.getInventory().setStackInSlot(slot, remainder);

                            if(!old.equals(remainder))
                                changedInv.set(true);
                        }
                    }
                });
            }
        }

        //Send packet
        if(changedInv.get() && !this.golem.getLevel().isClientSide()){
            Network.sendToTracking(this.golem, this.golem.createInvPacket());
        }

    }
}
