package net.lilith.crystalhearts.entities.ai;

import net.lilith.crystalhearts.entities.GolemEntity;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.navigation.PathNavigation;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.level.pathfinder.Path;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class GolemPickupItemsGoal extends Goal {

    private GolemEntity golem;
    private List<ItemEntity> targetItems = new ArrayList<>();

    public GolemPickupItemsGoal(GolemEntity entity){
        this.golem = entity;
        this.setFlags(EnumSet.of(Flag.MOVE));
    }

    @Override
    public boolean canUse() {
        for(int i = 0; i < golem.getInventory().getSlots(); ++i){
            if(!this.golem.getInventory().getStackInSlot(i).isEmpty()){
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean canContinueToUse() {

        if(this.targetItems.isEmpty())
            return false;

        return super.canContinueToUse();
    }

    @Override
    public void start() {
        this.targetItems = this.golem.getLevel().getEntitiesOfClass(ItemEntity.class, this.golem.getBoundingBox().inflate(16));
    }

    @Override
    public void tick() {
        super.tick();
        this.targetItems.removeIf(ent -> !ent.isAlive());

        PathNavigation nav = this.golem.getNavigation();

        if(nav == null || nav.isDone()){
            this.setNavToItem();
        }
    }

    public void setNavToItem(){
        if(!this.targetItems.isEmpty()) {
            Path path = this.golem.getNavigation().createPath(this.targetItems.get(0), 0);
            if(path != null)
                this.golem.getNavigation().moveTo(path, 1.0F);
        }
    }
}
