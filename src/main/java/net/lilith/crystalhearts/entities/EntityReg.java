package net.lilith.crystalhearts.entities;

import net.lilith.crystalhearts.Crystal;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.HashMap;

@Mod.EventBusSubscriber(modid = Crystal.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class EntityReg {

    public static final DeferredRegister<EntityType<?>> ENTITIES = DeferredRegister.create(ForgeRegistries.ENTITIES, Crystal.MODID);

    public static final RegistryObject<EntityType<BasicGolemEntity>> BASIC_GOLEM = ENTITIES.register("basic_golem", () -> create(BasicGolemEntity::new, MobCategory.CREATURE, 0.6F, 0.8F));

    public static <T extends Entity> EntityType<T> create(EntityType.EntityFactory<T> factory, MobCategory cat, float width, float height){
        return EntityType.Builder.of(factory, cat)
                .setTrackingRange(64)
                .sized(width, height)
                .setUpdateInterval(1)
                .build("");
    }

    @SubscribeEvent
    public static void registerAttributes(EntityAttributeCreationEvent event){
        event.put(EntityReg.BASIC_GOLEM.get(), BasicGolemEntity.createAttributes());
    }
}
