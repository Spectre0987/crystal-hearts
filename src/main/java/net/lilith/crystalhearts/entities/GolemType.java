package net.lilith.crystalhearts.entities;

public enum GolemType {
    CLAY(1),
    COPPER(3),
    IRON(5);

    private int invSize;

    GolemType(int invSize){
        this.invSize = invSize;
    }

    public int getInventorySize(){
        return this.invSize;
    }
}
