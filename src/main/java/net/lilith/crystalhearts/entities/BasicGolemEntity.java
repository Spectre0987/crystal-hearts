package net.lilith.crystalhearts.entities;

import net.lilith.crystalhearts.entities.ai.PlaceInChestGoal;
import net.lilith.crystalhearts.gems.GemReg;
import net.lilith.crystalhearts.gems.GemType;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class BasicGolemEntity extends GolemEntity{

    public BasicGolemEntity(EntityType<? extends Mob> type, Level level) {
        super(type, level);
    }

    public static AttributeSupplier createAttributes() {

        return Mob.createMobAttributes()
                .add(Attributes.MAX_HEALTH, 10.0F)
                .add(Attributes.MOVEMENT_SPEED, 0.23F)
                .add(Attributes.FOLLOW_RANGE, 16)
                .build();
    }

    @Override
    protected void registerGoals() {
        super.registerGoals();
        this.goalSelector.addGoal(3, new PlaceInChestGoal(this));

    }

    @Override
    public int getInventorySize() {
        return 1;
    }

    @Override
    protected InteractionResult mobInteract(Player pPlayer, InteractionHand pHand) {

        ItemStack stack = pPlayer.getItemInHand(pHand);

        if(!stack.isEmpty()){
            for(GemType type : GemReg.REGISTRY.get().getValues()){
                if(type.isFor(stack)){
                    this.setGem(type);
                    return InteractionResult.sidedSuccess(pPlayer.getLevel().isClientSide());
                }
            }
        }

        return super.mobInteract(pPlayer, pHand);
    }
}
