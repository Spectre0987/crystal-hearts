package net.lilith.crystalhearts.entities;

import net.lilith.crystalhearts.gems.Gem;
import net.lilith.crystalhearts.gems.GemReg;
import net.lilith.crystalhearts.gems.GemType;
import net.lilith.crystalhearts.network.GolemInvPacket;
import net.lilith.crystalhearts.network.Network;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.AnimationState;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nullable;
import java.util.HashMap;

public abstract class GolemEntity extends Mob {

    private BlockPos chestPos;
    private ItemStackHandler inventory;
    private Gem gem;

    public GolemEntity(EntityType<? extends Mob> type, Level level) {
        super(type, level);
        this.inventory = new ItemStackHandler(this.getInventorySize());
    }

    @Override
    public void baseTick() {
        super.baseTick();
        this.pickUpItems();

        if(this.gem != null)
            gem.tick();

    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
        this.inventory.deserializeNBT(nbt.getCompound("inv"));
        if(nbt.contains("chest_pos"))
            this.chestPos = BlockPos.of(nbt.getLong("chest_pos"));

        if(nbt.contains("gem")){
            this.setGem(GemReg.REGISTRY.get().getValue(new ResourceLocation(nbt.getString("gem"))));
        }

    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.put("inv", this.inventory.serializeNBT());
        if(this.chestPos != null && !this.chestPos.equals(BlockPos.ZERO))
            tag.putLong("chest_pos", this.chestPos.asLong());

        if(gem != null){
            tag.putString("gem", GemReg.REGISTRY.get().getKey(this.gem.getType()).toString());
        }

        return tag;
    }

    public abstract int getInventorySize();

    public ItemStackHandler getInventory(){
        return this.inventory;
    }

    public void setChestPos(BlockPos pos){
        this.chestPos = pos;
    }

    public BlockPos getChestPos(){
        return this.chestPos;
    }

    public void setGem(@Nullable GemType type){

        //Remove old Gem Tasks
        if(this.gem != null){
            this.gem.removeCustomAI(this.goalSelector);
        }

        //Remove gem data if we're setting the type to null (Like removing the heart)
        if(type == null){
            this.gem = null;
            return;
        }

        //Setup new gem if not null
        this.gem = type.create(this);
        this.gem.registerCustomAI(this.goalSelector);
    }

    public Gem getGem(){
        return this.gem;
    }

    public void pickUpItems(){

        boolean changed = false;

        for(ItemEntity item : level.getEntitiesOfClass(ItemEntity.class, this.getBoundingBox().inflate(1.0F))){
            for(int i = 0; i < this.inventory.getSlots(); ++i){
                int firstCount = item.getItem().getCount();

                ItemStack leftOver = this.inventory.insertItem(i, item.getItem(), false);

                //Kill Item if we took it all
                if(leftOver.isEmpty()) {
                    item.kill();
                    changed = true;
                }
                //decrease item if we took some
                else if(item.getItem().getCount() < firstCount) {
                    item.getItem().shrink(firstCount - item.getItem().getCount());
                    changed = true;
                }

            }
        }

        if(changed && !level.isClientSide()){
            Network.sendToTracking(this, createInvPacket());
        }
    }
    public GolemInvPacket createInvPacket(){
        HashMap<Integer, ItemStack> map = new HashMap<>();
        for(int i = 0; i < inventory.getSlots(); ++i){
            ItemStack stack = inventory.getStackInSlot(i);
            if(!stack.isEmpty())
                map.put(i, stack);
        }
        return new GolemInvPacket(this.getId(), map);
    }

    public ItemStack getFirstStack(){
        for(int i = 0; i < inventory.getSlots(); ++i){
            if(!inventory.getStackInSlot(i).isEmpty())
                return inventory.getStackInSlot(i);
        }
        return ItemStack.EMPTY;
    }
}
