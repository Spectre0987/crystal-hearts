package net.lilith.crystalhearts;

import net.lilith.crystalhearts.blocks.BlockReg;
import net.lilith.crystalhearts.entities.EntityReg;
import net.lilith.crystalhearts.gems.GemReg;
import net.lilith.crystalhearts.items.ItemReg;
import net.lilith.crystalhearts.sidedhelpers.CommonSidedHelper;
import net.lilith.crystalhearts.sidedhelpers.ISidedHelper;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@net.minecraftforge.fml.common.Mod(Crystal.MODID)
public class Crystal {

    public static final String MODID = "crystalhearts";
    public static ISidedHelper SIDDED_HELPER = new CommonSidedHelper();

    public Crystal(){

        IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();

        ItemReg.ITEMS.register(bus);
        BlockReg.BLOCKS.register(bus);
        EntityReg.ENTITIES.register(bus);
        GemReg.GEMS.register(bus);
    }
}
