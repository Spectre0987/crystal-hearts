package net.lilith.crystalhearts.gems;

import net.lilith.crystalhearts.entities.GolemEntity;
import net.minecraft.world.entity.ai.goal.GoalSelector;

import java.awt.*;

public class NetherQuartzGem extends Gem{

    public NetherQuartzGem(GemType type, GolemEntity golem) {
        super(type, golem);
    }

    @Override
    public void registerCustomAI(GoalSelector selector) {

    }

    @Override
    public void removeCustomAI(GoalSelector selector) {

    }

    @Override
    public void tick() {

    }

    @Override
    public Color getGemColor() {
        return new Color(0xdb0cb9);
    }
}
