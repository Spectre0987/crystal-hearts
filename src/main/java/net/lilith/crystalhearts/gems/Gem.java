package net.lilith.crystalhearts.gems;

import net.lilith.crystalhearts.entities.GolemEntity;
import net.minecraft.world.entity.ai.goal.GoalSelector;

import java.awt.*;

public abstract class Gem {

    private final GolemEntity golem;
    private final GemType type;

    public Gem(GemType type, GolemEntity golem){
        this.golem = golem;
        this.type = type;
    }

    public GolemEntity getGolem(){
        return this.golem;
    }

    public GemType getType(){
        return this.type;
    }

    public abstract void registerCustomAI(GoalSelector selector);
    public abstract void removeCustomAI(GoalSelector selector);
    public abstract void tick();
    public abstract Color getGemColor();


}
