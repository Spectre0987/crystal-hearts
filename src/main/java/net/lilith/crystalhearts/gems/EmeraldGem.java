package net.lilith.crystalhearts.gems;

import net.lilith.crystalhearts.entities.GolemEntity;
import net.lilith.crystalhearts.entities.ai.GolemPickupItemsGoal;
import net.minecraft.world.entity.ai.goal.GoalSelector;

import java.awt.*;

public class EmeraldGem extends Gem{

    private GolemPickupItemsGoal pickup;

    public EmeraldGem(GemType type, GolemEntity golem) {
        super(type, golem);
    }

    @Override
    public void registerCustomAI(GoalSelector selector) {
        selector.addGoal(4, this.pickup = new GolemPickupItemsGoal(this.getGolem()));
    }

    @Override
    public void removeCustomAI(GoalSelector selector) {
        selector.removeGoal(pickup);
    }

    @Override
    public void tick() {

    }

    @Override
    public Color getGemColor() {
        return new Color(0x19621b);
    }
}
