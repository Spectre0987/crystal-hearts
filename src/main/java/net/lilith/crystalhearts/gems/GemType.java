package net.lilith.crystalhearts.gems;

import net.lilith.crystalhearts.entities.GolemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;

import java.util.function.Supplier;

public class GemType {

    IGemBuilder builder;
    Supplier<Ingredient> item;

    public GemType(Supplier<Ingredient> ing, IGemBuilder builder){
        this.item = ing;
        this.builder = builder;
    }

    public Gem create(GolemEntity golem){
        return this.builder.create(this, golem);
    }

    public boolean isFor(ItemStack stack){
        return this.item.get().test(stack);
    }


    @FunctionalInterface
    public interface IGemBuilder{
        Gem create(GemType type, GolemEntity goelm);
    }
}
