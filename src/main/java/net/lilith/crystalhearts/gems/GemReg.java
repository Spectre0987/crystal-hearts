package net.lilith.crystalhearts.gems;

import net.lilith.crystalhearts.Crystal;
import net.lilith.crystalhearts.helpers.Helper;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Supplier;

public class GemReg {

    public static final DeferredRegister<GemType> GEMS = DeferredRegister.create(Helper.createRL("gems"), Crystal.MODID);
    public static final Supplier<IForgeRegistry<GemType>> REGISTRY = GEMS.makeRegistry(() -> new RegistryBuilder<GemType>());

    public static final RegistryObject<GemType> NETHER_QUARTZ = GEMS.register("nether_quartz", () -> new GemType(() -> Ingredient.of(Items.QUARTZ), NetherQuartzGem::new));
    public static final RegistryObject<GemType> EMERALD = GEMS.register("emerald", () -> new GemType(() -> Ingredient.of(Items.EMERALD), EmeraldGem::new));

}
