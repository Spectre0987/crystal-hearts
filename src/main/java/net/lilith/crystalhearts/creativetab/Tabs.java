package net.lilith.crystalhearts.creativetab;

import net.lilith.crystalhearts.Crystal;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;

public class Tabs {

    public static final CreativeModeTab MAIN = new CreativeModeTab(Crystal.MODID + ".main") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(Items.IRON_BLOCK);
        }
    };
}
