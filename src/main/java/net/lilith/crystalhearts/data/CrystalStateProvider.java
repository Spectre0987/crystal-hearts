package net.lilith.crystalhearts.data;

import net.lilith.crystalhearts.Crystal;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.common.data.ExistingFileHelper;

public class CrystalStateProvider extends BlockStateProvider {

    public CrystalStateProvider(DataGenerator gen, ExistingFileHelper exFileHelper) {
        super(gen, Crystal.MODID, exFileHelper);
    }

    @Override
    protected void registerStatesAndModels() {

    }
}
