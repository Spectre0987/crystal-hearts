package net.lilith.crystalhearts.data;

import net.lilith.crystalhearts.Crystal;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.forge.event.lifecycle.GatherDataEvent;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, modid = Crystal.MODID)
public class DataGen {

    @SubscribeEvent
    public static void gatherData(GatherDataEvent event){
        event.getGenerator().addProvider(true, new CrystalLangProvider(event.getGenerator()));
        event.getGenerator().addProvider(true, new CrystalStateProvider(event.getGenerator(), event.getExistingFileHelper()));
    }
}
