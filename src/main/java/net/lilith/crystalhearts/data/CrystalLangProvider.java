package net.lilith.crystalhearts.data;

import java.util.Arrays;
import java.util.stream.Collectors;

import net.lilith.crystalhearts.Crystal;
import net.lilith.crystalhearts.blocks.BlockReg;
import net.lilith.crystalhearts.creativetab.Tabs;
import net.minecraft.data.DataGenerator;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.contents.TranslatableContents;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.common.data.LanguageProvider;
import net.minecraftforge.registries.ForgeRegistries;

public class CrystalLangProvider extends LanguageProvider {

    public CrystalLangProvider(DataGenerator gen) {
        super(gen, Crystal.MODID, "en_us");
    }

    @Override
    protected void addTranslations() {
        add(Tabs.MAIN.getDisplayName(), "Crystal Hearts");
        
        addAutomaticNames();
    }

    public void addAutomaticNames(){
        for (Item item : ForgeRegistries.ITEMS.getValues()) {
            if(ForgeRegistries.ITEMS.getKey(item).getNamespace().contentEquals(Crystal.MODID)) {
                if (item instanceof BlockItem blockItem) {
                    Block block = blockItem.getBlock();
                    add(block.getDescriptionId(), fixCapitalisations(ForgeRegistries.BLOCKS.getKey(block).getPath()));
                }
                else {
                    add(item.getDescriptionId(), fixCapitalisations(ForgeRegistries.ITEMS.getKey(item).getPath()));
                }
            }
        }
    }

    public void add(Component component, String trans){
        if(component instanceof TranslatableContents)
            add(((TranslatableContents)component).getKey(), trans);
        else add(component.getString(), trans);
    }
    
    public String fixCapitalisations(String text) {
        String original = text.trim().replace("    ", "").replace("_", " ").replace("/", ".");
        String output = Arrays.stream(original.split("\\s+"))
                .map(t -> t.substring(0,1).toUpperCase() + t.substring(1))
                .collect(Collectors.joining(" "));
        return output;
    }
}
