package net.lilith.crystalhearts.misc;

public class ObjectHolder<T> {

    private T val;

    public ObjectHolder(T val){
        this.val = val;
    }

    public T get(){
        return this.val;
    }

    public void set(T val){
        this.val = val;
    }
}
