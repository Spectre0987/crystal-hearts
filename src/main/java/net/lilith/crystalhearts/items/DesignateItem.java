package net.lilith.crystalhearts.items;

import net.lilith.crystalhearts.creativetab.Tabs;
import net.lilith.crystalhearts.entities.GolemEntity;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.ChestBlockEntity;
import net.minecraftforge.items.CapabilityItemHandler;

public class DesignateItem extends Item {

    public DesignateItem() {
        super(new Properties().tab(Tabs.MAIN).stacksTo(1));
    }

    @Override
    public InteractionResult useOn(UseOnContext context) {

        BlockEntity tile = context.getLevel().getBlockEntity(context.getClickedPos());
        if(tile != null){
            tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(cap -> {
                setChestPos(context.getItemInHand(), tile.getBlockPos());
                System.out.println("Chest pos is " + tile.getBlockPos());
            });
        }

        return InteractionResult.sidedSuccess(context.getLevel().isClientSide());
    }

    @Override
    public InteractionResult interactLivingEntity(ItemStack pStack, Player pPlayer, LivingEntity target, InteractionHand pUsedHand) {
        BlockPos pos = getChestPos(pStack);
        if(target instanceof GolemEntity && !BlockPos.ZERO.equals(pos)){
            ((GolemEntity)target).setChestPos(pos);
            System.out.println("Set chest pos");
            return InteractionResult.sidedSuccess(target.level.isClientSide());
        }
        return InteractionResult.PASS;
    }

    public static void setChestPos(ItemStack stack, BlockPos pos){
        stack.getOrCreateTag().putLong("chest", pos.asLong());
    }

    public static BlockPos getChestPos(ItemStack stack){
        if(stack.hasTag() && stack.getTag().contains("chest"))
            return BlockPos.of(stack.getTag().getLong("chest"));
        return BlockPos.ZERO;
    }
}
