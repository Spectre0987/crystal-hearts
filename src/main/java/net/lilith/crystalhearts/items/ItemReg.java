package net.lilith.crystalhearts.items;

import net.lilith.crystalhearts.Crystal;
import net.minecraft.world.item.Item;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ItemReg {

    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, Crystal.MODID);

    public static final RegistryObject<DesignateItem> DEED = ITEMS.register("deed", DesignateItem::new);

}
