package net.lilith.crystalhearts.network;

import net.lilith.crystalhearts.Crystal;
import net.lilith.crystalhearts.helpers.Helper;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkRegistry;
import net.minecraftforge.network.PacketDistributor;
import net.minecraftforge.network.simple.SimpleChannel;

@Mod.EventBusSubscriber(modid = Crystal.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class Network {

    public static int ID = 0;
    public static final String VERSION = "1.0";
    public static final SimpleChannel INSTANCE = NetworkRegistry.newSimpleChannel(Helper.createRL("main"), () -> VERSION, VERSION::equals, VERSION::equals);

    public static void register(){
        INSTANCE.registerMessage(id(), GolemInvPacket.class, GolemInvPacket::encode, GolemInvPacket::decode, GolemInvPacket::handle);
    }

    public static int id(){
        return ++ID;
    }

    @SubscribeEvent
    public static void registerEvent(FMLCommonSetupEvent event){
        register();
    }

    public static void sendToClient(ServerPlayer player, Object message){
        INSTANCE.sendTo(message, player.connection.getConnection(), NetworkDirection.PLAY_TO_CLIENT);
    }

    public static void sendToTracking(Entity e, Object mes){
        INSTANCE.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.with(() -> e), mes);
    }
}
