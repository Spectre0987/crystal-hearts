package net.lilith.crystalhearts.network;

import net.lilith.crystalhearts.Crystal;
import net.lilith.crystalhearts.entities.GolemEntity;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.network.NetworkEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class GolemInvPacket {

    private HashMap<Integer, ItemStack> inv = new HashMap<>();
    private int entityID;

    public GolemInvPacket(int entityID, HashMap<Integer, ItemStack> stack){
        this.entityID = entityID;
        this.inv.putAll(stack);
    }

    public static void encode(GolemInvPacket mes, FriendlyByteBuf buf){
        buf.writeInt(mes.entityID);
        buf.writeInt(mes.inv.size());
        for(Map.Entry<Integer, ItemStack> entry : mes.inv.entrySet()){
            buf.writeInt(entry.getKey());
            buf.writeItem(entry.getValue());
        }
    }

    public static GolemInvPacket decode(FriendlyByteBuf buf){
        int entityID = buf.readInt();
        int size = buf.readInt();

        HashMap<Integer, ItemStack> temp = new HashMap<>();

        for(int i = 0; i < size; ++i){
            temp.put(buf.readInt(), buf.readItem());
        }
        return new GolemInvPacket(entityID, temp);
    }

    public static void handle(GolemInvPacket mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {
            Entity e = Crystal.SIDDED_HELPER.getClientWorld().getEntity(mes.entityID);
            if(e instanceof GolemEntity){
                ItemStackHandler inv = ((GolemEntity)e).getInventory();
                for(int i = 0; i < inv.getSlots(); ++i){
                    if(mes.inv.containsKey(i)){
                        inv.setStackInSlot(i, mes.inv.get(i));
                    }
                    else inv.setStackInSlot(i, ItemStack.EMPTY);
                }
            }
        });
        context.get().setPacketHandled(true);
    }
}
