package net.lilith.crystalhearts.helpers;

import net.lilith.crystalhearts.Crystal;
import net.minecraft.resources.ResourceLocation;

public class Helper {

    public static ResourceLocation createRL(String path){
        return new ResourceLocation(Crystal.MODID, path);
    }

}
