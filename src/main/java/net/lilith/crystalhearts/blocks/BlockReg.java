package net.lilith.crystalhearts.blocks;

import java.util.function.Supplier;

import net.lilith.crystalhearts.Crystal;
import net.lilith.crystalhearts.creativetab.Tabs;
import net.lilith.crystalhearts.items.ItemReg;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class BlockReg {

    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, Crystal.MODID);

    public static final RegistryObject<Block> GOLEM_MOLD = register("golem_mold", GolemMoldBlock::new);
    
    /** Register the block and its block item to the main creative tab*/
    private static <T extends Block> RegistryObject<T> register(String id, Supplier<T> blockSupplier) {
        RegistryObject<T> registryObject = BLOCKS.register(id, blockSupplier);
        ItemReg.ITEMS.register(id, () -> new BlockItem(registryObject.get(), new Item.Properties().tab(Tabs.MAIN)));
        return registryObject;
    }

    /** Register the block and its block item to the specified creative tab*/
    private static <T extends Block> RegistryObject<T> register(String id, Supplier<T> blockSupplier, CreativeModeTab itemGroup) {
        RegistryObject<T> registryObject = BLOCKS.register(id, blockSupplier);
        ItemReg.ITEMS.register(id, () -> new BlockItem(registryObject.get(), new Item.Properties().tab(itemGroup)));
        return registryObject;
    }
}
