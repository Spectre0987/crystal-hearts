package net.lilith.crystalhearts.sidedhelpers;

import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;

public interface ISidedHelper {

    Level getClientWorld();
    Player getClientPlayer();

}
