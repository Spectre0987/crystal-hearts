package net.lilith.crystalhearts.client.renderers;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Vector3f;
import net.lilith.crystalhearts.client.models.golem.GolemModel;
import net.lilith.crystalhearts.entities.GolemEntity;
import net.lilith.crystalhearts.helpers.Helper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.block.model.ItemTransforms;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.LivingEntityRenderer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.item.ItemStack;

public class GolemRenderer extends LivingEntityRenderer<GolemEntity, GolemModel> {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/entity/golem/clay.png");

    public GolemRenderer(EntityRendererProvider.Context context) {
        super(context, new GolemModel(context.bakeLayer(GolemModel.LAYER_LOCATION)), 0.3F);
    }

    @Override
    public void render(GolemEntity pEntity, float pEntityYaw, float pPartialTicks, PoseStack pMatrixStack, MultiBufferSource pBuffer, int pPackedLight) {
        super.render(pEntity, pEntityYaw, pPartialTicks, pMatrixStack, pBuffer, pPackedLight);

        pMatrixStack.pushPose();
        for(int i = 0; i < pEntity.getInventory().getSlots(); ++i){
            ItemStack stack = pEntity.getInventory().getStackInSlot(i);
            if(!stack.isEmpty()){

                pMatrixStack.mulPose(Vector3f.YP.rotationDegrees(-Mth.lerp(pPartialTicks, pEntity.yBodyRotO, pEntity.yBodyRot)));
                pMatrixStack.translate(0, 0.2, 0.5);
                BakedModel model = Minecraft.getInstance().getItemRenderer().getModel(stack, null, null, OverlayTexture.NO_OVERLAY);
                Minecraft.getInstance().getItemRenderer().render(stack, ItemTransforms.TransformType.THIRD_PERSON_RIGHT_HAND, false, pMatrixStack, pBuffer, pPackedLight, OverlayTexture.NO_OVERLAY, model);
                break;
            }
        }
        pMatrixStack.popPose();

    }

    @Override
    public ResourceLocation getTextureLocation(GolemEntity pEntity) {
        return TEXTURE;
    }

    @Override
    protected boolean shouldShowName(GolemEntity pEntity) {
        return false;
    }
}
