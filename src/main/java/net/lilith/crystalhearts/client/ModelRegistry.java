package net.lilith.crystalhearts.client;

import net.lilith.crystalhearts.Crystal;
import net.lilith.crystalhearts.client.models.golem.GolemModel;
import net.lilith.crystalhearts.client.renderers.GolemRenderer;
import net.lilith.crystalhearts.entities.EntityReg;
import net.lilith.crystalhearts.sidedhelpers.ClientSiddedHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Mod.EventBusSubscriber(value = Dist.CLIENT, modid = Crystal.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModelRegistry {

    @SubscribeEvent
    public static void register(FMLClientSetupEvent event){
        Crystal.SIDDED_HELPER = new ClientSiddedHelper();
    }

    @SubscribeEvent
    public static void registerModelLayer(EntityRenderersEvent.RegisterLayerDefinitions event){
        event.registerLayerDefinition(GolemModel.LAYER_LOCATION, GolemModel::createBodyLayer);
    }

    @SubscribeEvent
    public static void registerModels(EntityRenderersEvent.RegisterRenderers event){
        event.registerEntityRenderer(EntityReg.BASIC_GOLEM.get(), GolemRenderer::new);
    }
}
