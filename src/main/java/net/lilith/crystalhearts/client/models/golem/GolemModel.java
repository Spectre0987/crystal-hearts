package net.lilith.crystalhearts.client.models.golem;// Made with Blockbench 4.2.4
// Exported for Minecraft version 1.17 - 1.18 with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.lilith.crystalhearts.Crystal;
import net.lilith.crystalhearts.entities.GolemEntity;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;

import java.awt.*;

public class GolemModel extends HierarchicalModel<GolemEntity> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(Crystal.MODID, "golem"), "main");
	private final ModelPart head;
	private final ModelPart torso;
	private final ModelPart right_arm;
	private final ModelPart left_arm;
	private final ModelPart right_leg;
	private final ModelPart left_leg;

	private final ModelPart gem;

	private final ModelPart root;

	private Color gemColor;

	public GolemModel(ModelPart root) {
		this.head = root.getChild("head");
		this.torso = root.getChild("torso");
		this.right_arm = root.getChild("right_arm");
		this.left_arm = root.getChild("left_arm");
		this.right_leg = root.getChild("right_leg");
		this.left_leg = root.getChild("left_leg");
		this.gem = this.torso.getChild("gem");
		this.gem.skipDraw = true;
		this.root = root;
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition head = partdefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 16).addBox(-2.0F, -4.0F, -2.0F, 4.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 15.0F, 0.25F));

		PartDefinition jaw = head.addOrReplaceChild("jaw", CubeListBuilder.create().texOffs(16, 8).addBox(-2.5F, 1.65F, -2.35F, 5.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(20, 12).addBox(-2.5F, -0.35F, -0.35F, 5.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 16).addBox(-2.5F, 0.65F, -2.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 10).addBox(1.5F, 0.65F, -2.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 8).addBox(-0.5F, 0.65F, -2.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -2.65F, -0.15F));

		PartDefinition eyes = head.addOrReplaceChild("eyes", CubeListBuilder.create(), PartPose.offset(0.0F, 8.75F, -0.5F));

		PartDefinition l_eye = eyes.addOrReplaceChild("l_eye", CubeListBuilder.create().texOffs(12, 16).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(1.0F, -11.5F, -1.5F));

		PartDefinition r_eye = eyes.addOrReplaceChild("r_eye", CubeListBuilder.create().texOffs(15, 17).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.0F, -11.5F, -1.5F));

		PartDefinition torso = partdefinition.addOrReplaceChild("torso", CubeListBuilder.create(), PartPose.offset(-0.5F, 16.9021F, 0.3465F));

		PartDefinition belly_r1 = torso.addOrReplaceChild("belly_r1", CubeListBuilder.create().texOffs(0, 8).addBox(-2.5F, 0.25F, -3.35F, 6.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition chest_r1 = torso.addOrReplaceChild("chest_r1", CubeListBuilder.create().texOffs(0, 0).addBox(-3.5F, -1.75F, -2.0F, 7.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.5F, 0.0979F, -0.3465F, -0.3054F, 0.0F, 0.0F));

		PartDefinition neck_r1 = torso.addOrReplaceChild("neck_r1", CubeListBuilder.create().texOffs(16, 16).addBox(1.0F, -2.75F, -0.35F, 3.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -0.0763F, -0.4202F, 0.4363F, 0.0F, 0.0F));

		PartDefinition gem = torso.addOrReplaceChild("gem", CubeListBuilder.create(), PartPose.offset(0.25F, 0.0979F, -2.0965F));

		PartDefinition gem_r1 = gem.addOrReplaceChild("gem_r1", CubeListBuilder.create().texOffs(0, 0).addBox(-0.25F, -1.25F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.3054F, 0.0F, 0.0F));

		PartDefinition right_arm = partdefinition.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(22, 0).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 6.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-4.5F, 16.0F, 0.0F));

		PartDefinition left_arm = partdefinition.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(14, 22).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 6.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(4.5F, 16.0F, 0.0F));

		PartDefinition right_leg = partdefinition.addOrReplaceChild("right_leg", CubeListBuilder.create().texOffs(0, 24).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 5.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.5F, 21.0F, 0.0F));

		PartDefinition left_leg = partdefinition.addOrReplaceChild("left_leg", CubeListBuilder.create().texOffs(22, 22).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 5.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(1.75F, 21.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 32, 32);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		head.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		torso.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);

		if(this.gemColor != null){
			poseStack.pushPose();
			torso.translateAndRotate(poseStack);
			this.gem.render(poseStack, vertexConsumer, packedLight, packedOverlay, this.gemColor.getRed() / 255.0F, this.gemColor.getGreen() / 255.0F, this.gemColor.getRed() / 255.0F, 1.0F);
			poseStack.popPose();
		}

		right_arm.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		left_arm.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		right_leg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		left_leg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public ModelPart root() {
		return this.root;
	}

	@Override
	public void setupAnim(GolemEntity pEntity, float pLimbSwing, float pLimbSwingAmount, float pAgeInTicks, float pNetHeadYaw, float pHeadPitch) {

		float speed = (float) Mth.clamp(pEntity.getDeltaMovement().lengthSqr() * 100.0F, 0.0F, 1.0F);
		this.animateWalking(speed, pAgeInTicks, !pEntity.getFirstStack().isEmpty());
		this.animateHead(pNetHeadYaw, pHeadPitch);


		if(pEntity.getGem() != null){
			this.gemColor = pEntity.getGem().getGemColor();
		}
	}

	public void animateHead(float yaw, float pitch){
		this.head.yRot = (float)Math.toRadians(yaw);
		this.head.xRot = (float) Math.toRadians(pitch);
	}

	public void animateWalking(float speed, float ticks, boolean hasItem){
		float swing = (float)Math.toRadians(Math.sin(ticks * 0.5) * speed * 30);

		this.left_leg.xRot = swing;
		this.right_leg.xRot = -swing;

		if(hasItem){
			this.right_arm.xRot = -(float)Math.toRadians(70);
			this.left_arm.xRot = -(float)Math.toRadians(70);
			this.right_arm.yRot = -(float)Math.toRadians(20);
			this.left_arm.yRot = (float)Math.toRadians(20);
		}
		else {
			this.right_arm.xRot = swing;
			this.left_arm.xRot = -swing;
			this.right_arm.yRot = 0;
			this.left_arm.yRot = 0;
		}

	}
}